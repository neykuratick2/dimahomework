package practice;

import org.checkerframework.checker.units.qual.A;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneBook {

    private final Map<String, String> phoneBook = new TreeMap<>();



    public void addContact(String phone, String name) {
        // проверьте корректность формата имени и телефона
        // (рекомедуется написать отдельные методы для проверки является строка именем/телефоном)
        // если такой номер уже есть в списке, то перезаписать имя абонента
        if (validName(name) && validPhoneNumber(phone)) {
            phoneBook.put(phone, name);
            System.out.println("Номер добавлен");
        } if (phoneBook.containsValue(phone)) {
            phoneBook.replace(phone, name);
            System.out.println("Контакт отредактирован");
        } else if (phoneBook.containsValue(name)) {
            phoneBook.replace(phone + " , " + phone, name);
        }
    }

    public String getContactByPhone(String phone) {
        // формат одного контакта "Имя - Телефон"
        // если контакт не найдены - вернуть пустую строку
        if (phoneBook.containsKey(phone)) {
            return phoneBook.get(phone) + " - " + phone;
                } else {
            return "";
            }
        }


    public Set<String> getContactByName(String name) {
        // формат одного контакта "Имя - Телефон"
        // если контакт не найден - вернуть пустой TreeSet
        Set<String> contactSet = new TreeSet<>();
        String contact = "";
        if (phoneBook.containsValue(name)) {
            for (String phone : phoneBook.keySet()) {
                if (phoneBook.get(phone).equals(name)) {
                    contact = name + " - " + phone;
                    contactSet.add(contact);
                }
                return contactSet;
            }
        }
        return new TreeSet<>();
    }

    private boolean contactExists(String contactName) {
        return phoneBook.get(contactName) != null;
    }

    public Set<String> getAllContacts() {
        // формат одного контакта "Имя - Телефон"
        // если контактов нет в телефонной книге - вернуть пустой TreeSet

        TreeSet<String> getContacts = new TreeSet<>();
        HashMap<String, ArrayList<String>> knownKeys = new HashMap<>();  // Создали удобную буфер-мапу, типа реестра


        for (Map.Entry<String , String> entry : phoneBook.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            // проверяем на дубликаты контактов и добавляем в буфер существующих имён
            if (knownKeys.get(value) == null ) {
                knownKeys.put(value, new ArrayList<>(Collections.singletonList(key)));
            } else {
                knownKeys.get(value).add(key);
            }
        }


        // теперь итерируемся по созданной мапе с удобным форматом (Ключ: String, Значение: массив из стрингов)
        for(Map.Entry<String, ArrayList<String>> entry : knownKeys.entrySet()) {
            String name = entry.getKey();
            ArrayList<String> phoneNumbers = entry.getValue();
            StringBuilder contact = new StringBuilder(name + " - ");  // делаем общее начало строчки вывода контрактов

            // а тут решаем, выводить несколько номеров для одного контакта, или только один
            if (phoneNumbers.size() > 1 ) {
                // если несколько, то просто итерируемся по массиву номеров у этого контакта и добавляем их в строку
                for (String number : phoneNumbers) { contact.append(number).append(", "); }
                contact.delete(contact.length() - 2, contact.length()); // удаляем последнюю лишнюю подстрочку ", "
            } else {
                // если номер только один, добавляем его в финальную строчку текущего контакта
                contact.append(phoneNumbers.get(0));
            }

            // добавляем в финальный список вывода контактов
            getContacts.add(contact.toString());
        }

        return getContacts;
    }

    public boolean validName(String name) {
        Pattern pattern = Pattern.compile("[а-яА-Яa-zA-Z]+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public boolean validPhoneNumber(String phone) {
        Pattern pattern = Pattern.compile("\\d{11}");
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    // для обхода Map используйте получение пары ключ->значение Map.Entry<String,String>
    // это поможет вам найти все ключи (key) по значению (value)
    /*
        for (Map.Entry<String, String> entry : map.entrySet()){
            String key = entry.getKey(); // получения ключа
            String value = entry.getValue(); // получения ключа
        }
    */
}